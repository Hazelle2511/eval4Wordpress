<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'eFBBlsl1jhP+kx81ykZZhLLGUsmolevbk7jYaZJUF1HhWclJfzuxKOzrzmiEYWqAwWHa47czefNdLddn2xnx0w==');
define('SECURE_AUTH_KEY',  'GDAa0Soc3ijzfvo2fmMWHFtVlEYtdigrL8Mw00CL3b4+5CWTD6ggOeNnhltp/Bz7UV9CgiWeUtAa8245kuiQqg==');
define('LOGGED_IN_KEY',    'HJ8XBedfSCqvwasXd0NrumOs8n7kYQe/NoEvxgBzua0uRuocwIpIsENlI0SSH4Eu6tyUyWY1i0y6AsxVFOZj4w==');
define('NONCE_KEY',        'nnCIWGE9PJxZ0uWzRFbJ9cgMeNcDLPUkshBvS1vAFPqniB0c5qDt1HvCin2Y+aBdboQWdkGZwOdnb/nTPBxB+A==');
define('AUTH_SALT',        'Chmm3suWvt4xaOXEaa5PFvICbSrTymDvUmTiKpA3mygex96iFyjAeqKkyceauGZpOCW4uvF4In7qDsudQ6a/IA==');
define('SECURE_AUTH_SALT', 'As7mZ49Zf04buF6uvlEpG6hjWsjoMWFSmZ68NjeMvNBMlz9Y+TK8s3y0LECXqFVS8Yt4yNy9AgRZHZ0z/6Rq/A==');
define('LOGGED_IN_SALT',   'ppBYRuszI92nxCMO/hpg8HG5oL2KtHiHH7yDiUxXdIpU2wudq5PdZ5lbrkupqz+aWkcRcTknw+2y6tUriNMxLg==');
define('NONCE_SALT',       'IwWSFdZadQGhTK4HlxrgT9BeTFUbu9T/HrlT3p8z/aH58xwYak4YYBCDyT2sEvkvCmifIoUVHHQGnHlndDfLXQ==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
